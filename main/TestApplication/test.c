/*
 * main.c
 *
 *  Created on: 2015-01-03
 *      Author: brendan
 */

#include "unit.h"
#include "FreeRTOS.h"
#include "task.h"
#include "Test_Satellite.h"
#include "Test_Supervisor.h"
#include "Test_WODSupervisor.h"
#include "Test_Commands.h"
#include "Test_TelecommandSupervisor.h"
#include <stdio.h>

#define MAX_STACK_DEPTH 1000
#define TEST_RUNNER_TASK_PRIO ( tskIDLE_PRIORITY + 1 )

static void testRunnerTask(void *arg)
{
	printf("\nTesting...\n\n");
	INIT_TESTING();
	RUN_TEST_SUITE(Satellite);
	RUN_TEST_SUITE(Supervisor);
	RUN_TEST_SUITE(WODSupervisor);
	//RUN_TEST_SUITE(TelecommandSupervisor);
	RUN_TEST_SUITE(Commands);
	printf("\n\nPlease manually kill this process\n");
	for(;;);
}


int main(int argc, char **args)
{
	xTaskCreate(testRunnerTask,
				(signed char const * const) "Test Runner Task",
				MAX_STACK_DEPTH,
				NULL,
				TEST_RUNNER_TASK_PRIO,
				NULL);
	vTaskStartScheduler();
	return 0;
}

