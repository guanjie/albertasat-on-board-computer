extern "C" {
#include "gps_info.h"
}
#include <CppUTest/TestHarness.h>
#include <string.h>


TEST_GROUP(GpsInfoTest){
};

static const size_t FIELD_COUNT = 17;
static void fieldList(char* fields[], GpsInfo* info){
    char* list[] = {
		info->utc,
		info->lat,
		info->ns,
		info->lon,
		info->ew,
		info->posFixInd,
		info->satsUsed,
		info->hdop,
		info->alt,
		info->fixMode,
		info->fixType,
		info->pdop,
		info->vdop,
		info->status,
		info->speed,
		info->course,
		info->date
	};
    for (size_t i = 0; i < FIELD_COUNT; i++){
    	fields[i] = list[i];
    }
}

static void assignValues(GpsInfo* info, const char* values[]){
	char* fields[FIELD_COUNT];
	fieldList(fields, info);
	for (size_t i = 0; i < FIELD_COUNT; i++){
		strcpy(fields[i], values[i]);
	}
}

static void checkValues(const char* exp[], GpsInfo* info){
	char* fields[FIELD_COUNT];
	fieldList(fields, info);
	for (size_t i = 0; i < FIELD_COUNT; i++){
		STRCMP_EQUAL(exp[i], fields[i]);
	}
}
// Should have a GpsInfo struct that holds lat, long and time
// position fix indicator
TEST(GpsInfoTest, Struct){
	GpsInfo info;
	const char* exp[] = {
        "165545.466",
	    "5330.5435",
        "N",
        "11330.5234",
        "E",
        "1",
        "12",
        "123.3",
        "99999.9",
        "A",
        "3",
        "1253.5",
        "4596.5",
        "V",
        "999.99",
        "323.5",
        "031214"
	};
	assignValues(&info, exp);
	checkValues(exp, &info);
}

//Should init with all NULL
TEST(GpsInfoTest, Init){
	GpsInfo info;
	GpsInfo_init(&info);
	char* fields[FIELD_COUNT];
	fieldList(fields, &info);
	for (size_t i = 0; i < FIELD_COUNT; i++){
		STRCMP_EQUAL("",fields[i]);
	}
}

//Should be able to assign after an init
TEST(GpsInfoTest, AssignAfterInit){
	GpsInfo info;
	const char* exp[] = {
			"165545.466",
			"1330.5234",
			"N",
			"15330.5435",
			"E",
			"1",
	        "14",
	        "124.3",
	        "99989.9",
	        "M",
	        "2",
	        "1255.5",
	        "4599.5",
	        "A",
	        "888.88",
	        "123.5",
	        "041214"
	};
	GpsInfo_init(&info);
	assignValues(&info, exp);
	checkValues(exp, &info);
}

