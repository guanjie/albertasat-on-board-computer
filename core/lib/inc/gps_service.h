/*
 * gps_service.h
 *
 *  Created on: Nov 22, 2014
 *      Author: albertasat
 */

#ifndef GPS_SERVICE_H_
#define GPS_SERVICE_H_

#include "gps_info.h"
//#include <stdbool.h>


//const size_t GPS_INFO_MAX_LENGTH = 10;
/*

typedef struct {
	const char* lat;
	const char* lon;
	const char* utcTime;
} GpsInfo;
*/
//bool GpsService_stringsEqual(const char* a, const char* b);
void GpsService_setFix(GpsInfo* info);
void GpsService_fix(GpsInfo* info);

#endif /* GPS_SERVICE_H_ */
