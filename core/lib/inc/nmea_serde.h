/*
 * nmea_serde.h
 *
 *  Created on: Nov 24, 2014
 *      Author: albertasat
 */

#ifndef NMEA_SERDE_H_
#define NMEA_SERDE_H_

#include "gps_info.h"

enum {
	NMEA_COMMAND_SIZE  = 7, // e.g., $GPGGA
	NMEA_CHECKSUM_SIZE = 3, // e.g., "*18", no trailing comma
	NMEA_LINE_END_SIZE = 2, // "\r\n", no trailing comma
	NMEA_GGA_MAX_SIZE = NMEA_COMMAND_SIZE // "$GPGGA"
			          + GPS_INFO_UTC_SIZE
	                  + GPS_INFO_LAT_SIZE
	                  + GPS_INFO_NS_SIZE
	                  + GPS_INFO_LON_SIZE
	                  + GPS_INFO_EW_SIZE
	                  + GPS_INFO_POS_FIX_IND_SIZE
	                  + GPS_INFO_SATS_USED_SIZE
	                  + GPS_INFO_HDOP_SIZE
	                  + GPS_INFO_ALT_SIZE
	                  + GPS_INFO_ALT_UNITS_SIZE
	                  + 1 // Geoid separation (not supported), so null
	                  + 2 // Geoid separation units: "M"
	                  + 1 // Age of diff corr: null
	                  + 4 // diff ref station id: "0000", no trailing comma
	                  + NMEA_CHECKSUM_SIZE
	                  + NMEA_LINE_END_SIZE,

    // e.g., "$PSRF100,1,38400,8,1,2"
	//        1234567890123456789012
	NMEA_100_MAX_SIZE = 23,
	NMEA_RMC_MAX_SIZE = NMEA_COMMAND_SIZE // "$GPRMC"
	                  + GPS_INFO_UTC_SIZE
	                  + GPS_INFO_STATUS_SIZE
	                  + GPS_INFO_LAT_SIZE
	                  + GPS_INFO_NS_SIZE
	                  + GPS_INFO_LON_SIZE
	                  + GPS_INFO_EW_SIZE
	                  + GPS_INFO_SPEED_SIZE
	                  + GPS_INFO_COURSE_SIZE
	                  + GPS_INFO_DATE_SIZE
	                  + 3 // always ",,A", no comma before checksum
	                  + NMEA_CHECKSUM_SIZE
	                  + NMEA_LINE_END_SIZE
};

typedef enum {
	NMEA_BAUD_1200 = 1200,
	NMEA_BAUD_2400 = 2400,
	NMEA_BAUD_4800 = 4800,
	NMEA_BAUD_9600 = 9600,
	NMEA_BAUD_19200 = 19200,
	NMEA_BAUD_38400 = 38400
} NmeaBaud;
typedef enum {
	NMEA_DATA_BITS_7 = 7,
	NMEA_DATA_BITS_8 = 8
} NmeaDataBits;
typedef enum {
	NMEA_STOP_BITS_0 = 0,
	NMEA_STOP_BITS_1 = 1
} NmeaStopBits;
typedef enum {
	NMEA_PARITY_NONE = 0,
	NMEA_PARITY_ODD = 1,
	NMEA_PARITY_EVEN = 2
} NmeaParity;

typedef struct {
    NmeaBaud baud;
    NmeaDataBits dataBits;
    NmeaStopBits stopBits;
    NmeaParity parity;
} NmeaSerialPortSettings;

void NmeaSerde_serGga(char serialized[], GpsInfo* gpsInfo);
void NmeaSerde_deserGga(GpsInfo* gpsInfo, char serialized[]);
void NmeaSerde_ser100(char serialized[], const NmeaSerialPortSettings*);
#endif /* NMEA_SERDE_H_ */
