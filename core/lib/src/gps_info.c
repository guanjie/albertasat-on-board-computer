/*
 * gps_info.c
 *
 *  Created on: Nov 22, 2014
 *      Author: albertasat
 */
#include "gps_info.h"
#include <string.h>

//! Set array of char* to ""
static void clear(char** a, size_t count){
	size_t i;
	for (i = 0; i < count; i++){
		strcpy(a[i], "");
	}
}
//! Initialize all strings to ""
void GpsInfo_init(GpsInfo* info){
	size_t fieldCount = 17;
	char* fields[] = {
			info->utc,
			info->lat,
			info->ns,
			info->lon,
			info->ew,
			info->posFixInd,
	        info->satsUsed,
	        info->hdop,
	        info->alt,
	        info->fixMode,
	        info->fixType,
	        info->pdop,
	        info->vdop,
	        info->status,
	        info->speed,
	        info->course,
	        info->date
	};
	clear(fields, fieldCount);
}
