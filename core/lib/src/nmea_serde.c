/*
 * nmea_serde.c
 *
 *  Created on: Nov 24, 2014
 *      Author: albertasat
 */
#include "nmea_serde.h"
#include <stdbool.h>
#include <string.h>

// Format:
/*
 $GPGGA,utc(hhmmss.sss),lat(ddmm.mmmm),N/Sind(N or S),lon(dddmm.mmmm),
 E/W ind (E or W),Position Fix Indicator(0-3), Satellites used(0-12),
 HDOP(n.n?), alt, units(M), Geoid separation (blank?), units(M),
 age of diff corr, diff ref station id, checksum, <CR><LF>
 */
//!Serialize gpsInfo into a string
/*!
  \param serialized [out] output buffer for the serialized string.  Must
      be big enough for the output
  \param gpsInfo the info to serialize
 */
void NmeaSerde_serGga(char serialized[], GpsInfo* gpsInfo){
    strcpy (serialized,"$GPGGA,");
    strncat(serialized,gpsInfo->utc, GPS_INFO_UTC_SIZE);
    strcat (serialized,",");
    strncat(serialized,gpsInfo->lat, GPS_INFO_LAT_SIZE);
    strcat (serialized,",");
    strncat(serialized,gpsInfo->ns,GPS_INFO_NS_SIZE);
    strcat (serialized,",");
    strncat(serialized,gpsInfo->lon, GPS_INFO_LON_SIZE);
    strcat (serialized,",");
    strncat(serialized,gpsInfo->ew, GPS_INFO_EW_SIZE);
}
//! Copy characters from the start of src to delim into dest, up to count, not including the final \0
//! a final \0 is added.  If delim not found, copies to \0 or count characters, whichever is less.
/*!
  \param dest [out] result of copy; must be at least size count + 1
  \param src [in] string to copy from; if NULL, "" is copied into dest
  \param delim [in] delimiter to look for
  \param count [in] max number of characters to copy
  \returns pointer to one past the delimiter or NULL if delimiter not found;
*/
static char* parseToDelim(char* dest, const char* src, int delim, size_t count ){
	char* delimLocation;
	size_t copyCount;
	bool delimFound;
	if (src == NULL){
		strcpy(dest, "");
		return NULL;
	}
	delimLocation = strchr(src, delim);
	if (delimLocation == NULL){ // didn't find delim before end of string, so look for \0
		delimFound = false;
		delimLocation = strchr(src,0);
	} else {
		delimFound = true;
	}
	copyCount = delimLocation - src;
	if (copyCount > count){
		copyCount = count;
	}
	dest[copyCount] = '\0';
	memcpy(dest, src, copyCount);
    if (!delimFound){
		return (char*)NULL;
    }
	return delimLocation + 1;
}
//! Deserialize a string into gpsInfo
/*!
  \param gpsInfo [out] results of the deserialization
  \param serialized the serialize data
 */
void NmeaSerde_deserGga(GpsInfo* gpsInfo, char serialized[]){
	int delim = ',';
	char command[NMEA_COMMAND_SIZE];
	//size_t serializedEnd = serialized + strlen(serialized);
	//char serializedCopy[NMEA_GGA_MAX_SIZE];
	//strncpy(serialized, serializedCopy, NMEA_GGA_MAX_SIZE - 1);
	char* restOfString = serialized;
	restOfString = parseToDelim(command, restOfString, delim, NMEA_COMMAND_SIZE - 1);
    restOfString = parseToDelim(gpsInfo->utc,       restOfString, delim, GPS_INFO_UTC_SIZE - 1);
    restOfString = parseToDelim(gpsInfo->lat,       restOfString, delim, GPS_INFO_LAT_SIZE - 1);
    restOfString = parseToDelim(gpsInfo->ns,        restOfString, delim, GPS_INFO_NS_SIZE  - 1);
    restOfString = parseToDelim(gpsInfo->lon,       restOfString, delim, GPS_INFO_LON_SIZE - 1);
    restOfString = parseToDelim(gpsInfo->ew,        restOfString, delim, GPS_INFO_EW_SIZE  - 1);
    restOfString = parseToDelim(gpsInfo->posFixInd, restOfString, delim, GPS_INFO_POS_FIX_IND_SIZE -1);
    restOfString = parseToDelim(gpsInfo->satsUsed,  restOfString, delim, GPS_INFO_SATS_USED_SIZE - 1);
    restOfString = parseToDelim(gpsInfo->hdop,      restOfString, delim, GPS_INFO_HDOP_SIZE - 1);
    restOfString = parseToDelim(gpsInfo->alt,       restOfString, delim, GPS_INFO_ALT_SIZE - 1);
}

//! Set serial port command serialization (id 100)
void NmeaSerde_ser100(char serialized[], const NmeaSerialPortSettings* settings){
	strcpy(serialized,"$PSRF100,1,"); // always starts with this 1 = NMEA protocol
	switch (settings->baud){
	case NMEA_BAUD_1200:
		strcat(serialized, "1200,");
		break;
	case NMEA_BAUD_2400:
		strcat(serialized, "2400,");
		break;
	case NMEA_BAUD_4800:
		strcat(serialized, "4800,");
		break;
	case NMEA_BAUD_9600:
		strcat(serialized, "9600,");
		break;
	case NMEA_BAUD_19200:
		strcat(serialized, "19200,");
		break;
	case NMEA_BAUD_38400:
		strcat(serialized, "38400,");
		break;
	default:
		strcat(serialized,"error,");
	}

    switch (settings->dataBits){
    case NMEA_DATA_BITS_7:
    	strcat(serialized, "7,");
    	break;
    case NMEA_DATA_BITS_8:
    	strcat(serialized, "8,");
    	break;
    default:
    	strcat(serialized, "e,");
    }

    switch (settings->stopBits){
    case NMEA_STOP_BITS_0:
    	strcat(serialized, "0,");
    	break;
    case NMEA_STOP_BITS_1:
    	strcat(serialized, "1,");
    	break;
    default:
    	strcat(serialized, "e,");
    }

    switch (settings->parity){
    case NMEA_PARITY_NONE:
    	strcat(serialized,"0");
    	break;
    case NMEA_PARITY_ODD:
    	strcat(serialized, "1");
    	break;
    case NMEA_PARITY_EVEN:
    	strcat(serialized, "2");
    	break;
    default:
    	strcat(serialized,"e");
    }
}


