/*
 * gps_service.c
 *
 *  Created on: Nov 22, 2014
 *      Author: albertasat
 */
#include "gps_service.h"
#include <string.h>

static GpsInfo data;

//! Set the latest fix data
void GpsService_setFix(GpsInfo* fix){
	strncpy(data.utc, fix->utc, GPS_INFO_UTC_SIZE);
	strncpy(data.lat, fix->lat, GPS_INFO_LAT_SIZE);
	strncpy(data.lon, fix->lon, GPS_INFO_LON_SIZE);
}

//! Get the last fix data
/*!
  \!param fix [out] The fix data is placed here.
 */
void GpsService_fix(GpsInfo* fix){
    strncpy(fix->utc, data.utc, GPS_INFO_UTC_SIZE);
    strncpy(fix->lat, data.lat, GPS_INFO_LAT_SIZE);
    strncpy(fix->lon, data.lon, GPS_INFO_LON_SIZE);
}

