/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file PowerSafe.h
 * @author Brendan Bruner
 * @date 2015-01-07
 */

#ifndef POWERSAFE_H_
#define POWERSAFE_H_

#include "Class.h"
#include "State.h"

/**
 * @struct PowerSafe
 * @brief
 * 		Behavior when battery voltage is too low for continued operation.
 * @extends State
 */
Class(PowerSafe) Extends(State)
	Data
		int _switchingVoltage;
	Methods
		int (*getSwitchingVoltage)(PowerSafe *);
EndClass;

#endif /* POWERSAFE_H_ */
