/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file CommandCollectWOD.h
 * @author Brendan Bruner
 * @date 2015-01-10
 */

#ifndef COMMANDCOLLECTWOD_H_
#define COMMANDCOLLECTWOD_H_

#include "Class.h"
#include "Command.h"

/**
 * @struct CommandCollectWOD
 * @brief
 * 		Used to invoke the collection of WOD.
 * @extends Command
 */
Class(CommandCollectWOD) Extends(Command)
	Data
	Methods
EndClass;

#endif /* COMMANDCOLLECTWOD_H_ */
