/*
 * Copyright (C) 2015 Collin Cupido
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file TeleCommandSupervisor.h
 * @author Collin Cupido
 * @date 2015-01-14
 *
 * Prototypes WODSupervisor class
 */

#ifndef TELECOMMANDSUPERVISOR_H_
#define TELECOMMANDSUPERVISOR_H_

#include "Class.h"
#include "Supervisor.h"

#define TELECOMMAND_SUPERVISOR_STACK_SIZE 2

/**
 * @struct TelecommandSupervisor
 * @brief
 * 		Queries for commands and invokes the Satellite object to execute
 * 		them.
 * @remark
 * 		Overrides the method supervisorTask() of the Supervisor class.
 * @sa Supervisor::_supervisorTask
 * @extends Supervisor
 */
Class(TelecommandSupervisor) Extends(Supervisor)
	Data
	Methods
EndClass;

#endif /* TELECOMMANDSUPERVISOR_H_ */
