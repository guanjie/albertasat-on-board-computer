/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file Detumble.h
 * @author Brendan Bruner
 * @date 2015-01-07
 */

#ifndef DETUMBLE_H_
#define DETUMBLE_H_

#include "Class.h"
#include "State.h"

/**
 * @struct PowerOn
 * @brief
 * 		Behavior when power is first turned on.
 * @extends State
 */
Class(Detumble) Extends(State)
	Data
	Methods
EndClass;

#endif /* DETUMBLE_H_ */
