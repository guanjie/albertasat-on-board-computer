/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file Command.c
 * @author Brendan Bruner
 * @date 2015-01-07
 */

#include "Command.h"
#include "Satellite.h"


/*****************************************************************************/
/* Methods																	 */
/*****************************************************************************/
/**
 * @brief
 * 		Empty method
 * @details
 * 		This methods does nothing. It only exists to prevent undefined program
 * 		behavior. Subclasses will override this method.
 * @public @memberof Command
 */
static void execute(Command *obj, Satellite *sat)
{
	return;
}

/*****************************************************************************/
/* Constructor																 */
/*****************************************************************************/
/**
 * @brief
 * 		Constructor for Command objects.
 * @public @memberof Command
 */
Constructor(Command)
{
	LinkMethod(execute);
}
