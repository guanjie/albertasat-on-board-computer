/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file CommandSetStatePowerSafe.c
 * @author Brendan Bruner
 * @date 2015-01-07
 */

#include "CommandSetStatePowerSafe.h"
#include "Satellite.h"
#include <stdio.h>

/*****************************************************************************/
/* Methods																	 */
/*****************************************************************************/
/**
 * @brief
 * 		Force the Satellite into the PowerSafe State.
 * @details
 * 		Force the Satellite into the PowerSafe State. This command does not
 * 		force it to stay in PowerSafe state, the command only forces it to
 * 		transistion into the PowerSafe state.
 * @param obj
 * 		A pointer to the CommandSetStatePowerSafe object invoking this method.
 * @param sat
 * 		The Satellite object which will be affected by this command.
 * @sa Command::execute()
 * @public @memberof CommandSetStatePowerSafe
 */
static void execute(Command *obj, Satellite *sat)
{
	sat->setCurrentState(sat, sat->getPowerSafeState(sat));
	printf("State set to PowerSafe.\n");
}

/*****************************************************************************/
/* Constructor																 */
/*****************************************************************************/
Constructor(CommandSetStatePowerSafe)
{
	Super(Command);
	OverrideMethod(Command, execute);
}
