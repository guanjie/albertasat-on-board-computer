/*
 * main.cpp
 *
 *  Created on: 2014-11-22
 *      Author: brendan
 */

#include "CppUTest/CommandLineTestRunner.h"

int main(int argc, char **argv)
{
	return CommandLineTestRunner::RunAllTests(argc, argv);
}

IMPORT_TEST_GROUP(Satellite);

