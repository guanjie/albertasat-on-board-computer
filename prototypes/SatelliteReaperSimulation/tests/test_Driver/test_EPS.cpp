/*
 * test_EPS.cpp
 *
 *  Created on: 2014-11-23
 *      Author: brendan
 */
/*
extern "C" {
#include "EPS.h"
#include "EPSHardwareMockUp.h"
#include <unistd.h>
}

#include "CppUTest/TestHarness.h"

TEST_GROUP(EPS)
{
	EPS eps;
	EPSHardwareMockUp epsHardware;

	void setup()
	{
		newEPS(&eps);
		newEPSHardwareMockUp(&epsHardware);
	}
};

*
 * Test that the getBatteryVoltage method returns a reasonable value.
 * Gomspace specifies their power board's battery voltage is between
 * 0 and 9.5.

TEST(EPS, testGetBatteryLevel)
{

	 * Gomspace's power board stores battery voltage as a uint16_t
	 * with units of mV.

	uint16_t minBatteryVoltage, maxBatteryVoltage;
	uint16_t actualBatteryVoltage;
	bool batteryVoltageReasonable;

	minBatteryVoltage = 0;
	maxBatteryVoltage = 9500;
	actualBatteryVoltage = eps.getBatteryVoltage(&eps);

	batteryVoltageReasonable = 	(actualBatteryVoltage > minBatteryVoltage) &&
								(actualBatteryVoltage < maxBatteryVoltage);

	CHECK_TRUE(batteryVoltageReasonable);
}

*
 * Tests the ability of the EPS driver to return a valid housekeeping
 * structure as defined in io/nanopower.h, the driver provided by
 * Gomspace for their power supply board.

TEST(EPS, testGetHouseKeeping)
{
	 Data structure for housekeeping defined in io/nanopower.h
	eps_hk_t *hk;
	uint8_t battmode;
	bool battmodeReasonable;

	 Set the pointer to housekeeping struct
	hk = eps.getHouseKeeping(&eps);

	 The structure member battmode is offset quite far from the pointer
	 houseKeeping - about 47 bytes. It can only be 4 values: 0, 1, 2,
	 and 3. Therefore, there is a high probability that if the
	 getHouseKeeping function is not working, then this memory location
	 will not be 0, 1, 2, or 3.
	battmode = hk->battmode
	battmodeReasonable = (battmode == 0 || battmode == 1 || battmode == 2 || battmode == 3);

	CHECK_TRUE(battmodeReasonable);

	 TODO: Investigate how memory is allocated for the eps_hk_t struct. If
	 there is one global struct allocated in .data then change the value of
	 hk->battmode to, say, 23, then recall getHouseKeeping and redo the
	 same test.
}

*
 * Tests that the EPS can be sent a configuration file.

TEST(EPS, testpushConfiguration)
{

}

*
 * Test that the EPS class will correctly reset watchdog
 * timers on the EPS board.

TEST(EPS, testWatchdogHandling)
{
	uint32_t epsWatchdog_before, epsWatchdog_after;
	int i;

	 Portable delay ensures watchdog has been ticking for a bit.
	for(i = 0; i < 10^4; ++i){}

	epsWatchdog_before = epsHardware.getWatchdogTimeRemaining(&epsHardware);
	eps.resetWatchdog(&eps);
	epsWatchdog_after = epsHardware.getWatchdogTimeRemaining(&epsHardware);

	CHECK_TRUE((epsWatchdog_before > epsWatchdog_after));
}
*/
