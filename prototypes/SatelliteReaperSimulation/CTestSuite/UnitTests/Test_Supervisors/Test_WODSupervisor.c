/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file Test_WODSupervisor.c
 * @author Brendan Bruner
 * @date 2015-01-09
 */

#include "WODSupervisor.h"
#include "Test_WODSupervisor.h"
#include "StateMockUp.h"

#define TEST_WOD_STACK_SIZE 5
#define TEST_WOD_PRIO	(tskIDLE_PRIORITY + 1)

TEST(wod_collection_task)
{
	WODSupervisor sup;
	Satellite sat;
	StateMockUp testState;

	newWODSupervisor(&sup);
	newSatellite(&sat);
	newStateMockUp(&testState);

	sat.setCurrentState(&sat, (State *) &testState);
	((Supervisor *) &sup)->setSatellite((Supervisor *) &sup, &sat);
	((Supervisor *) &sup)->setPeriod((Supervisor *) &sup, (period_t) 25);

	ASSERT("wod_collection_task 1", testState.collectWODCount(&testState) == 0);
	((Supervisor *) &sup)->initSupervisorTask((Supervisor *) &sup,
												"wod_collection_task_test",
												TEST_WOD_STACK_SIZE,
												TEST_WOD_PRIO);
	vTaskDelay(75 / portTICK_RATE_MS);
	vTaskDelete(*((Supervisor *) &sup)->getTaskHandle((Supervisor *) &sup));

	/* Assert that wod was collected over the last 75ms */
	ASSERT("wod_collection_task 2", testState.collectWODCount(&testState) > 0);

	return TEST_END;
}

TEST_SUITE(WODSupervisor)
{
	ADD_TEST(wod_collection_task);
	return TEST_END;
}
