/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file SupervisorMockUp.c
 * @author Brendan Bruner
 * @date 2015-01-07
 */

#include "SupervisorMockUp.h"

/*****************************************************************************/
/* Methods																	 */
/*****************************************************************************/
static void _supervisorTask(Supervisor *obj)
{
	for(;;)
	{
		++((SupervisorMockUp *) obj)->data.counter;
		vSupervisorTaskDelay(obj->data._period);
	}
}

static void kill(SupervisorMockUp *obj)
{
	vTaskDelete(((Supervisor *) obj)->data._taskHandle);
	((Supervisor *) obj)->data._taskHandle = NULL;
	((Supervisor *) obj)->data._isTaskCreated = TASK_NOT_CREATED;
}

/*****************************************************************************/
/* Constructor																 */
/*****************************************************************************/
Constructor(SupervisorMockUp)
{
	Super(Supervisor);
	LinkMethod(kill);
	OverrideMethod(Supervisor, _supervisorTask);

	obj->data.counter = 0;
	((Supervisor *) obj)->data._period = 10;
}
