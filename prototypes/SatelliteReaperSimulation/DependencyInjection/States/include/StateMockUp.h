/*
 * StateMockUp.h
 *
 *  Created on: 2014-12-19
 *      Author: brendan
 *
 * This class provides an access point for testing States by
 * enabling testing of methods in the Satellite class which delegate work
 * to the a State object.
 */

#ifndef STATEMOCKUP_H_
#define STATEMOCKUP_H_

#include "Class.h"
#include "Command.h"
#include "State.h"

Class(StateMockUp) Extends(State)
	Data
		int _payloadCount, _WODCount, _transmitCount, _softCount, _hardCount;
	Methods
		int (*collectPayloadDataCount)(StateMockUp *);
		int (*collectWODCount)(StateMockUp *);
		int (*transmitDataCount)(StateMockUp *);
		int (*executeSoftTelecommandCount)(StateMockUp *);
		int (*executeHardTelecommandCount)(StateMockUp *);
EndClass;

#endif /* STATEMOCKUP_H_ */
