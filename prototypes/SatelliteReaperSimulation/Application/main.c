
#include <stdio.h>

#include "FreeRTOS.h"
#include "task.h"

#include "Satellite.h"
#include "EPS.h"
#include "WODSupervisor.h"
#include "TelecommandSupervisor.h"


#define WOD_SUPERVISOR_PRIO ( tskIDLE_PRIORITY + 1)
/* MS period. */
#define WOD_SUPERVISOR_PERIOD 2000
#define WOD_SUPERVISOR_NAME "WODSupervisor"

#define TELECOMMAND_SUPERVISOR_PRIO ( tskIDLE_PRIORITY + 2)
/* MS period. */
#define TELECOMMAND_SUPERVISOR_PERIOD 2000
#define TELECOMMAND_SUPERVISOR_NAME "TeleCommandSupervisor"

#define MY_ADDRESS 8

int main(void)
{
	Satellite sat;
	EPS eps;
	COMM comm;
	WODSupervisor wod;
	TelecommandSupervisor telec;

	newSatellite(&sat);
	newEPS(&eps);
	newCOMM(&comm);
	newWODSupervisor(&wod);
	newTelecommandSupervisor(&telec);

	/* Set the EPS driver the Satellite object uses and the current state
	 * of the Satellite object to be the PowerSafe state. */
	sat.setEPSDriver(&sat, &eps);
	sat.setCOMMDriver(&sat,&comm);
	sat.setCurrentState(&sat, sat.getPowerSafeState(&sat));

	/* Set the period of the WODSupervisor, the Satellite object the Supervisor
	 * has access to, and initialize it. */
	((Supervisor *) &wod)->setPeriod((Supervisor *) &wod, WOD_SUPERVISOR_PERIOD);
	((Supervisor *) &wod)->setSatellite((Supervisor *) &wod, &sat);
	((Supervisor *) &wod)->initSupervisorTask((Supervisor *) &wod,
												WOD_SUPERVISOR_NAME,
												WOD_SUPERVISOR_STACK_SIZE,
												WOD_SUPERVISOR_PRIO);
	/* Set the period of the WODSupervisor, the Satellite object the Supervisor
	* has access to, and initialize it. */
	((Supervisor *) &telec)->setPeriod((Supervisor *) &telec, TELECOMMAND_SUPERVISOR_PERIOD);
	((Supervisor *) &telec)->setSatellite((Supervisor *) &telec, &sat);
	((Supervisor *) &telec)->initSupervisorTask((Supervisor *) &telec,
													TELECOMMAND_SUPERVISOR_NAME,
													TELECOMMAND_SUPERVISOR_STACK_SIZE,
													TELECOMMAND_SUPERVISOR_PRIO);

	vTaskStartScheduler();
	return 0;
}
