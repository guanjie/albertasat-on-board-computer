/*
 * gps_em406a.h
 *
 * THIS IS AN INCOMPLETE DRIVER USED ONLY TO HELP CONFIRM BUILD SETTINGS ARE WORKING.
 * The intention is that this file will be deleted once other production code is in place.
 * In particular, this is not the GPS device we will be using for Albertasat.
 */
#ifndef GPS_EM406A_H_
#define GPS_EM406A_H_

#include <stddef.h>
enum {
	GPS_DRIVER_MAX_MESSAGE_SIZE = 100
};
typedef struct GpsDriver {
	int temp;
} GpsDriver;

extern size_t (*GpsDriver_getChars)(GpsDriver* gpsDriver, char* buffer);
extern void (*GpsDriver_putChars)(GpsDriver* gpsDriver, char* msg, size_t len);


#endif /* GPS_EM406A_H_ */
