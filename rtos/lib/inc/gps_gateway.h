/*
 * gps_gateway.h
 *
 *  Created on: Nov 29, 2014
 *      Author: albertasat
 */

#ifndef GPS_GATEWAY_H_
#define GPS_GATEWAY_H_

#include "gps_em406a.h" // test device Neil happens to have.  Can be replaced when convenient.
#include <stdint.h>

typedef struct GpsMsgBuffer GpsMsgBuffer;
struct GpsMsgBuffer {
    char chars[GPS_DRIVER_MAX_MESSAGE_SIZE];
    size_t len;
};

uint32_t GpsGateway_init(GpsDriver* gpsDriver);
void GpsGateway_fromGpsMsgBuffer(GpsMsgBuffer* msg);
void GpsGateway_queueFromGpsIsr();
void GpsGateway_receiveFromGps();
void GpsGateway_queueToGps(const char* msg);
void GpsGateway_receiveToGpsIsr();


#endif /* GPS_GATEWAY_H_ */
