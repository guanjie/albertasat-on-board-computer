/*
 * gps_gateway.c
  *
 * THIS IS AN INCOMPLETE CODE ONLY USED TO CONFIRM BUILD SETTINGS ARE WORKING.
 * Production code will likely have to be modified for the GPS device we intend to deploy.
 *
 */
#include "gps_gateway.h"
#include "rtosport.h"
#include "FreeRTOS.h"
#include "queue.h"
#include <stdbool.h>
#include <string.h>


static struct {
	GpsDriver* gpsDriver;
	xQueueHandle fromGpsQueue;
	xQueueHandle toGpsQueue;
	GpsMsgBuffer fromGpsMsgBuffer;
	GpsMsgBuffer toGpsMsgBuffer;
	GpsMsgBuffer fromGpsIsrBuffer;
	GpsMsgBuffer toGpsIsrBuffer;
	bool cr;
} data;

//! Initializes GpsGatewayIn, including creating the queue that communicates with gpsDriver
/*!
 \param[in] gpsDriver the driver that reads from the GPS device
 \return 0 if initialized ok, >0 if an error. 1 = could not create in queue, 2 = could not create out queue
 */
uint32_t GpsGateway_init(GpsDriver* gpsDriver){
	data.gpsDriver = gpsDriver;
	data.fromGpsQueue = xQueueCreate(1, sizeof(GpsMsgBuffer));
	data.toGpsQueue = xQueueCreate(1, sizeof(GpsMsgBuffer));
	if (data.fromGpsQueue == NULL){
		return 1; //could not create  in queue
	}
	if (data.toGpsQueue == NULL){
		return 2;
	}
	data.fromGpsMsgBuffer.len = 0;
	data.fromGpsIsrBuffer.len = 0;
	data.cr = false;
	return 0;
}

//! Gets the contents of the last msg received from the gps
/*!
 \param [out] msg The message struct, null terminated (and \r\n stripped)
 */
void GpsGateway_fromGpsMsgBuffer(GpsMsgBuffer* msg){
	memcpy(msg->chars, data.fromGpsMsgBuffer.chars, data.fromGpsMsgBuffer.len);
	// null terminate
	msg->chars[data.fromGpsMsgBuffer.len] = '\0';
	msg->len = data.fromGpsMsgBuffer.len;
}

//! Writes a complete message from the GPS to a queue; to be called as the ISR.
/*!
  Call GpsDriver_getChars to get the incoming chars from the Gps. Once a
  complete message is received (that is \r\n is detected), the message is written
  to an internal queue (picked up by GpsGatewayIn_readGpsQueue).
  The \r\n is stripped from the message before it is placed in the queue.
 */
void GpsGateway_queueFromGpsIsr(){
	char gpsBuffer[GPS_DRIVER_MAX_MESSAGE_SIZE];
	portBASE_TYPE taskWoken = pdFALSE;
	size_t i;
    size_t count = GpsDriver_getChars(data.gpsDriver, gpsBuffer);
    for (i = 0; i < count; i++){
        char c = gpsBuffer[i];
        data.fromGpsIsrBuffer.chars[data.fromGpsIsrBuffer.len] = c;
        // looking for \r\n at end of message, so it can be queued
        if (c == '\r'){
        	data.cr = true;
        } else {
        	if (data.cr && c == '\n'){
        		data.fromGpsIsrBuffer.len = data.fromGpsIsrBuffer.len - 1; // strip out \r\n
            	xQueueSendFromISR(data.fromGpsQueue, &data.fromGpsIsrBuffer, &taskWoken);
            	data.fromGpsIsrBuffer.len = -1; // reset the buffer
            }
        	data.cr = false;
        }
        data.fromGpsIsrBuffer.len++;
    }
	portEND_SWITCHING_ISR(taskWoken); // standard call at end of ISR after xQueueSendFromISR
}

//! Receive a complete messages that has been queued from the GPS
void GpsGateway_receiveFromGps(){
	xQueueReceive(data.fromGpsQueue, &data.fromGpsMsgBuffer, portMAX_DELAY);
}

//! Task that call GpsGateway_receiveFromGps repeatedly
void GpsGateway_receiveFromGpsTask( void *pParameters )
{
     for( ;; ){
    	 GpsGateway_receiveFromGps();
	 }
}

//! queue up a message to the GPS
/*
\* param [out] msg null terminated message, without \r\n
 */
void GpsGateway_queueToGps(const char* msg){
	// TODO: allow passing in max time to wait to queue
    data.toGpsMsgBuffer.len = strnlen(msg, GPS_DRIVER_MAX_MESSAGE_SIZE);
    memcpy(data.toGpsMsgBuffer.chars, msg, data.toGpsMsgBuffer.len);
	xQueueSend(data.toGpsQueue, &data.toGpsMsgBuffer, 0); // TODO: return this call's retval?
}
//! Pick up latest queued msg and write to GPS
void GpsGateway_receiveToGpsIsr(){
	portBASE_TYPE taskWoken = pdFALSE;
    xQueueReceiveFromISR(data.toGpsQueue, &data.toGpsIsrBuffer, &taskWoken);
    GpsDriver_putChars(data.gpsDriver, data.toGpsIsrBuffer.chars, data.toGpsIsrBuffer.len);
    GpsDriver_putChars(data.gpsDriver, "\r\n", 2);
    portEND_SWITCHING_ISR(taskWoken); // standard call after xQueueReceiveFromISR
}

