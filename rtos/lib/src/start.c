/*
Initialize tasks and start up the scheduler
*/
#include "time_service_updater.h"
#include "rtosport.h"
/* FreeRTOS.org includes. */
#include "FreeRTOS.h"
#include "task.h"
/* Used as a loop counter to create a very crude delay. */
#define mainDELAY_LOOP_COUNT          ( 0xfffff )
/* The placeholder task functions - will be removed */
void vTask1( void *pvParameters );
void vTask2( void *pvParameters );


int start(void) {
    xTaskCreate(     vTask1,          /* Pointer to the function that implements the task. */
                        "Task 1",     /* Text name for the task.  This is to facilitate debugging only. */
                        240,          /* Stack depth in words. */
                        NULL,          /* We are not using the task parameter. */
                        1,               /* This task will run at priority 1. */
                        NULL );          /* We are not using the task handle. */
    /* Create the other task in exactly the same way. */
    xTaskCreate( vTask2, "Task 2", 240, NULL, 1, NULL );
    xTaskCreate( vTimeServiceUpdater, "Time Service Updater Task", 240, NULL, 1, NULL );
    /* Start the scheduler so our tasks start executing. */
    vTaskStartScheduler();
    /* If all is well we will never reach here as the scheduler will now be
    running.  If we do reach here then it is likely that there was insufficient
    heap available for the idle task to be created. */
    for( ;; );
    return 0;
}

/*-----------------------------------------------------------*/
void vTask1( void *pvParameters )
{
const char *pcTaskName = "Task 1 is running\n";
volatile unsigned long ul;
     /* As per most tasks, this task is implemented in an infinite loop. */
     for( ;; )
     {
		  vTaskDelay(1000 / portTICK_RATE_MS);
          /* Print out the name of this task. */
          //vPrintString( pcTaskName );
          printf( pcTaskName );
          /* Delay for a period. */
          //for( ul = 0; ul < mainDELAY_LOOP_COUNT; ul++ )
          //{
               /* This loop is just a very crude delay implementation.  There is
               nothing to do in here.  Later exercises will replace this crude
               loop with a proper delay/sleep function. */
          //}
     }
}
/*-----------------------------------------------------------*/
void vTask2( void *pvParameters )
{
const char *pcTaskName = "Task 2 is running\n";
volatile unsigned long ul;
     /* As per most tasks, this task is implemented in an infinite loop. */
     for( ;; )
     {
          /* Print out the name of this task. */
          //vPrintString( pcTaskName );
		  vTaskDelay(1000 / portTICK_RATE_MS);
		  printf(pcTaskName);
    	 /* Delay for a period. */
         // for( ul = 0; ul < mainDELAY_LOOP_COUNT; ul++ )
          //{
               /* This loop is just a very crude delay implementation.  There is
               nothing to do in here.  Later exercises will replace this crude
               loop with a proper delay/sleep function. */
          //}
     }
}
/*-----------------------------------------------------------*/
void vApplicationMallocFailedHook( void )
{
     /* This function will only be called if an API call to create a task, queue
     or semaphore fails because there is too little heap RAM remaining. */
     for( ;; );
}
/*-----------------------------------------------------------*/
void vApplicationStackOverflowHook( xTaskHandle *pxTask, signed char *pcTaskName )
{
     /* This function will only be called if a task overflows its stack.  Note
     that stack overflow checking does slow down the context switch
     implementation. */
     for( ;; );
}
/*-----------------------------------------------------------*/
void vApplicationIdleHook( void )
{
     /* This example does not use the idle hook to perform any processing. */
}
/*-----------------------------------------------------------*/
void vApplicationTickHook( void )
{
     /* This example does not use the tick hook to perform any processing. */
}
