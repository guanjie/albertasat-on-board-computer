/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file COMM.h
 * @author Brendan Bruner
 * @date 2015-01-12
 */

#ifndef COMM_H_
#define COMM_H_

#include "Class.h"
#include "TelecommandPacket.h"
#include "TelemetryPacket.h"

/**
 * @struct COMM
 * @brief
 * 		Driver for nanocomm - communications subsystem.
 * @details
 * 		Provides an interface for talking to the communications subsystem. This
 * 		class starts a task to handle asynchronous data transfer between the
 * 		application code and communications subsystem.
 */
Class(COMM)
	Data
		struct telecommand_packet_t telecommandPacket;
	Methods
		struct telecommand_packet_t *(*blockOnTelecommand)(COMM *);
		struct telecommand_packet_t (*getCommand)(COMM *);
EndClass;

#endif /* COMM_H_ */
