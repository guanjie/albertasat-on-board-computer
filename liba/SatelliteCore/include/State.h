/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file State.h
 * @author Brendan Bruner
 * @date 2015-01-07
 */

#ifndef STATE_H_
#define STATE_H_

#include "Class.h"
#include "Command.h"

#define DETUMBLE_TEST_ID 0
#define POWER_ON_TEST_ID 1
#define POWER_SAFE_TEST_ID 2
#define SCIENCE_TEST_ID 3

/* Forward declare satellite class. */
Forward(Satellite);

/**
 * @struct State
 * @brief
 * 		Abstract class for states
 * @details
 * 		Abstract class for all State classes. This class prototypes the function pointers
 * 		which will be overrode by many subclasses and provides the most common implementations
 * 		for those function pointers. Methods defined in this class should
 * 		only be called through the Satellite class. Never directly.
 * @attention
 * 		All subclasses must be written thread safe, but do not have to be reentrant.
 */
Class(State)
	Data
		unsigned short _testID; /* For testing only. */
	Methods
		void (*collectPayloadData)(State *, Satellite *);
		void (*collectWOD)(State *, Satellite *);
		void (*transmitData)(State *, Satellite *);
		void (*executeSoftTelecommand)(State *, Command *, Satellite *, TelecommandSupervisor *);
		void (*executeHardTelecommand)(State *, Command *, Satellite *, TelecommandSupervisor *);
EndClass;

#endif /* STATE_H_ */
