/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file CommandSetStatePowerSafe.h
 * @author Brendan Bruner
 * @date 2015-01-07
 */

#ifndef COMMANDSETSTATEPOWERSAFE_H_
#define COMMANDSETSTATEPOWERSAFE_H_

#include "Class.h"
#include "Command.h"

/**
 * @struct CommandSetStatePowerSafe
 * @brief
 * 		Force the Satellite into the PowerSafe State.
 * @details
 * 		Force the Satellite into the PowerSafe State. This command does not
 * 		force it to stay in PowerSafe state, the command only forces it to
 * 		transistion into the PowerSafe state.
 * @extends Command
 */
Class(CommandSetStatePowerSafe) Extends(Command)
	Data
	Methods
EndClass;

#endif /* COMMANDSETSTATEPOWERSAFE_H_ */
