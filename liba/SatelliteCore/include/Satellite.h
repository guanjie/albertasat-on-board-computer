/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file Satellite.h
 * @author Brendan Bruner
 * @date 2015-01-06
 */

#ifndef SATELLITE_H_
#define SATELLITE_H_

#include "FreeRTOS.h"
#include "semphr.h"
#include "Class.h"
#include "Command.h"
#include "State.h"
#include "PowerOn.h"
#include "PostEjection.h"
#include "PreStartUpCharge.h"
#include "StartUp.h"
#include "PowerSafe.h"
#include "Detumble.h"
#include "Science.h"
#include "EPS.h"
#include "COMM.h"

/**
 * @struct Satellite
 * @brief
 * 		Access point to state behaviour.
 * @details
 * 		This class acts as the access point to state specific functionality. It
 * 		implements the OO state design pattern to do this. The current state of
 * 		the Satellite object is unknown to the application, all the application
 * 		has to do is call a method which is a function of the current state. The
 * 		current state object will have a method with the same name which will
 * 		be called. Depending which State object is the current state, the implementation
 * 		of the function may be different - giving state specific behavior.
 * @remark
 * 		All methods are thread safe, but not every method is reentrant.
 */
Class(Satellite)
	Data
		State *_currentState;
		PowerOn _powerOn;
		PowerSafe _powerSafe;
		Detumble _detumble;
		Science _science;
		/* TODO Satellite class has an instance of all other state classes. */
		EPS *_eps;
		COMM *_comm;
		xSemaphoreHandle _stateMutex, _driverMutex;
	Methods
		void (*setCurrentState)(Satellite *, State *);
		State *(*getCurrentState)(Satellite *);
		State *(*getPowerOnState)(Satellite *);
		State *(*getPostEjectionState)(Satellite *);
		State *(*getPreStartUpChargeState)(Satellite *);
		State *(*getStartUpState)(Satellite *);
		State *(*getPowerSafeState)(Satellite *);
		State *(*getDetumbleState)(Satellite *);
		State *(*getScienceState)(Satellite *);
		void (*setEPSDriver)(Satellite *, EPS *);
		EPS *(*getEPSDriver)(Satellite *);
		void (*setCOMMDriver)(Satellite *, COMM *);
		COMM *(*getCOMMDriver)(Satellite *);
		void (*collectPayloadData)(Satellite *);
		void (*collectWOD)(Satellite *);
		void (*transmitData)(Satellite *);
		void (*executeSoftTelecommand)(Satellite *, Command *, TelecommandSupervisor *);
		void (*executeHardTelecommand)(Satellite *, Command *, TelecommandSupervisor *);
EndClass;
#endif /* SATELLITE_H_ */
