/*
 * Copyright (C) 2015 Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file Satellite.c
 * @author Brendan Bruner
 * @date 2014-12-21
 */

#include "Satellite.h"
#include <stdio.h>
/*****************************************************************************/
/* State Methods 															 */
/*****************************************************************************/

/**
 * @details
 * 		Set the current State of the Satellite object.
 * @param obj
 * 		A pointer to the Satellite object invoking this method.
 * @param state
 * 		A pointer to the State object which will become the current
 * 		state.
 * @remark
 * 		Thread safe.
 * 		Non reentrant.
 * @public @memberof Satellite
 */
static void setCurrentState(Satellite *obj, State *state)
{
	xSemaphoreTake(obj->data._stateMutex, portMAX_DELAY);
	obj->data._currentState = state;
	xSemaphoreGive(obj->data._stateMutex);
}

/**
 * @details
 * 		Get a pointer to the current State of the Satellite object.
 * @param obj
 * 		A pointer to the Satellite object invoking this method.
 * @return
 * 		A pointer to the current State of the Satellite object.
 * @remark
 * 		Thread safe.
 * 		Non-reentrant.
 * @public @memberof Satellite
 */
static State *getCurrentState(Satellite *obj)
{
	State *state;

	xSemaphoreTake(obj->data._stateMutex, portMAX_DELAY);
	state = obj->data._currentState;
	xSemaphoreGive(obj->data._stateMutex);

	return state;
}

/**
 * @details
 * 		Get a pointer to an instance of the PowerOn (State) object.
 * @param obj
 * 		A pointer to the Satellite object invoking obj method.
 * @return
 * 		A pointer to a State object.
 * @remark
 * 		Thread safe.
 * 		Reentrant.
 * @public @memberof Satellite
 */
static State *getPowerOnState(Satellite *obj)
{
	return (State *) &obj->data._powerOn;
}

/**
 * @details
 * 		Get a pointer to an instance of the PostEjection (State) object.
 * @param obj
 * 		A pointer to the Satellite object invoking obj method.
 * @return
 * 		A pointer to a State object.
 * @remark
 * 		Thread safe.
 * 		Reentrant.
 * @public @memberof Satellite
 */
static State *getPostEjectionState(Satellite *obj)
{
	/* TODO: return PostEjection pointer */
	return (State *) NULL;
}

/**
 * @details
 * 		Get a pointer to an instance of the PreStartUpCharge (State) object.
 * @param obj
 * 		A pointer to the Satellite object invoking obj method.
 * @return
 * 		A pointer to a State object.
 * @remark
 * 		Thread safe.
 * 		Reentrant.
 * @public @memberof Satellite
 */
static State *getPreStartUpChargeState(Satellite *obj)
{
	/* TODO: return PreStartUpCharge pointer */
	return (State *) NULL;
}

/**
 * @details
 * 		Get a pointer to an instance of the StartUp (State) object.
 * @param obj
 * 		A pointer to the Satellite object invoking obj method.
 * @return
 * 		A pointer to a State object.
 * @remark
 * 		Thread safe.
 * 		Reentrant.
 * @public @memberof Satellite
 */
static State *getStartUpState(Satellite *obj)
{
	/* TODO: return StartUp pointer */
	return (State *) NULL;
}

/**
 * @details
 * 		Get a pointer to an instance of the PowerSafe (State) object.
 * @param obj
 * 		A pointer to the Satellite object invoking obj method.
 * @return
 * 		A pointer to a State object.
 * @remark
 * 		Thread safe.
 * 		Reentrant.
 * @public @memberof Satellite
 */
static State *getPowerSafeState(Satellite *obj)
{
	return (State *) &obj->data._powerSafe;
}

/**
 * @details
 * 		Get a pointer to an instance of the Detumble (State) object.
 * @param obj
 * 		A pointer to the Satellite object invoking obj method.
 * @return
 * 		A pointer to a State object.
 * @remark
 * 		Thread safe.
 * 		Reentrant.
 * @public @memberof Satellite
 */
static State *getDetumbleState(Satellite *obj)
{
	return (State *) &obj->data._detumble;
}

/**
 * @details
 * 		Get a pointer to an instance of the Science (State) object.
 * @param obj
 * 		A pointer to the Satellite object invoking obj method.
 * @return
 * 		A pointer to a State object.
 * @remark
 * 		Thread safe.
 * 		Reentrant.
 * @public @memberof Satellite
 */
static State *getScienceState(Satellite *obj)
{
	return (State *) &obj->data._science;
}

/*****************************************************************************/
/* Driver Methods 															 */
/*****************************************************************************/
/**
 * @details
 * 		Get a pointer to an instance of the EPS (Driver) object.
 * @param obj
 * 		A pointer to the Satellite object invoking the method.
 * @return
 * 		A pointer to the EPS driver being used.
 * @remark
 * 		Thread safe.
 * 		Non-reentrant.
 * @public @memberof Satellite
 */
static EPS *getEPSDriver(Satellite *obj)
{
	EPS *eps;

	xSemaphoreTake(obj->data._driverMutex, portMAX_DELAY);
	eps = obj->data._eps;
	xSemaphoreGive(obj->data._driverMutex);

	return eps;
}

/**
 * @brief
 * 		Sets the EPS Driver the Satellite object will use.
 * @param obj
 * 		A pointer to the Satellite object invoking the method.
 * @param eps
 * 		A pointer to the EPS object to use.
 * @remark
 * 		Thread safe.
 * 		Non-reentrant.
 * @public @memberof Satellite
 */
static void setEPSDriver(Satellite *obj, EPS *eps)
{
	xSemaphoreTake(obj->data._driverMutex, portMAX_DELAY);
	obj->data._eps = eps;
	xSemaphoreGive(obj->data._driverMutex);
}

/**
 * @details
 * 		Get a pointer to an instance of the COMM (Driver) object.
 * @param obj
 * 		A pointer to the Satellite object invoking the method.
 * @return
 * 		A pointer to the COMM driver being used.
 * @remark
 * 		Thread safe.
 * 		Non-reentrant.
 * @public @memberof Satellite
 */
static COMM *getCOMMDriver(Satellite *obj)
{
	COMM *comm;

	xSemaphoreTake(obj->data._driverMutex, portMAX_DELAY);
	comm = obj->data._comm;
	xSemaphoreGive(obj->data._driverMutex);

	return comm;
}

/**
 * @brief
 * 		Sets the COMM Driver the Satellite object will use.
 * @param obj
 * 		A pointer to the Satellite object invoking the method.
 * @param eps
 * 		A pointer to the COMM object to use.
 * @remark
 * 		Thread safe.
 * 		Non-reentrant.
 * @public @memberof Satellite
 */
static void setCOMMDriver(Satellite *obj, COMM *comm)
{
	xSemaphoreTake(obj->data._driverMutex, portMAX_DELAY);
	obj->data._comm = comm;
	xSemaphoreGive(obj->data._driverMutex);
}

/*****************************************************************************/
/* Function Methods 														 */
/*****************************************************************************/

/**
 * @details
 * 		Delegates work of collecting payload data to the current State.
 * @param obj
 * 		A pointer to the Satellite object obj method is being invoked from.
 * @remark
 * 		Thread safe.
 * 		Non reentrant.
 * @sa State::collectPayloadData
 * @public @memberof Satellite
 */
static void collectPayloadData(Satellite *obj)
{
	State *curState;

	curState = obj->getCurrentState(obj);
	curState->collectPayloadData(curState, obj);
}

/**
 * @details
 * 		Delegates work of collecting WOD to the current State.
 * @param obj
 * 		A pointer to the Satellite object obj method is being invoked from.
 * @remark
 * 		Thread safe.
 * 		Non reentrant.
 * @sa State::collectWOD
 * @public @memberof Satellite
 */
static void collectWOD(Satellite *obj)
{
	State *curState;

	curState = obj->getCurrentState(obj);
	curState->collectWOD(curState, obj);
}

/**
 * @details
 * 		Delegates work of transmitting data to the current state.
 * @param obj
 * 		A pointer to the Satellite object obj method is being invoked from.
 * @remark
 * 		Thread safe.
 * 		Non reentrant.
 * @sa State::transmitData
 * @public @memberof Satellite
 */
static void transmitData(Satellite *obj)
{
	State *curState;

	curState = obj->getCurrentState(obj);
	curState->transmitData(curState, obj);
}

/**
 * @details
 * 		Delegates work of executing commands which have a low priority
 * 		to the current state.
 * @param obj
 * 		A pointer to the Satellite object obj method is being invoked from.
 * @param command
 * 		A pointer to the Command subclass object to execute.
 * @param sup
 * 		A pointer to the TelecommandSupervisor invoking this method.
 * @remark
 * 		Thread safe.
 * 		Non reentrant.
 * @sa State::executeSoftTelecommand
 * @sa Command
 * @public @memberof Satellite
 */
static void executeSoftTelecommand(Satellite *obj, Command *command, TelecommandSupervisor *sup)
{
	State *curState;

	curState = obj->getCurrentState(obj);
	curState->executeSoftTelecommand(curState, command, obj, sup);
}

/**
 * @details
 * 		Delegates work of executing commands which have a high priority
 * 		to the current state.
 * @param obj
 * 		A pointer to the Satellite object obj method is being invoked from.
 * @param command
 * 		A pointer to the Command subclass object to execute.
 * @param sup
 * 		A pointer to the TelecommandSupervisor invoking this method.
 * @remark
 * 		Thread safe.
 * 		Non reentrant.
 * @sa State::executeHardTelecommand
 * @sa Command
 * @public @memberof Satellite
 */
static void executeHardTelecommand(Satellite *obj, Command *command, TelecommandSupervisor *sup)
{
	State *curState;

	curState = obj->getCurrentState(obj);
	curState->executeHardTelecommand(curState, command, obj, sup);
}

/*****************************************************************************/
/* Constructor																 */
/*****************************************************************************/
/**
 * @details
 * 		Constructor for satellite objects.
 * @remark
 * 		Thread safe.
 * 		Reentrant.
 * @public @memberof Satellite
 */
Constructor(Satellite)
{
	/* Link every class method */
	LinkMethod(setCurrentState);
	LinkMethod(getCurrentState);
	LinkMethod(getPowerOnState);
	LinkMethod(getPostEjectionState);
	LinkMethod(getPreStartUpChargeState);
	LinkMethod(getStartUpState);
	LinkMethod(getPowerSafeState);
	LinkMethod(getDetumbleState);
	LinkMethod(getScienceState);
	LinkMethod(collectPayloadData);
	LinkMethod(collectWOD);
	LinkMethod(transmitData);
	LinkMethod(executeSoftTelecommand);
	LinkMethod(executeHardTelecommand);
	LinkMethod(getEPSDriver);
	LinkMethod(setEPSDriver);
	LinkMethod(getCOMMDriver);
	LinkMethod(setCOMMDriver);

	/* Initialize the State objects */
	newPowerOn(&obj->data._powerOn);
	newPowerSafe(&obj->data._powerSafe);
	newDetumble(&obj->data._detumble);
	newScience(&obj->data._science);

	/* Initialize the semaphores */
	obj->data._stateMutex = xSemaphoreCreateMutex();
	obj->data._driverMutex = xSemaphoreCreateMutex();

	/* Check returned semaphore */
	if( obj->data._stateMutex == NULL || obj->data._driverMutex == NULL)
	{
		/* TODO: error handle failed mutex creation */
		/* TODO: refactor into separate if statement for each mutex */
	}
	else
	{
		xSemaphoreGive(obj->data._stateMutex);
		xSemaphoreGive(obj->data._driverMutex);
	}
}

/* These globals are for testing only. Do not use them in application code. */
void (*setCurrentStateAddress)(Satellite *, State *) = &setCurrentState;
State *(*getCurrentStateAddress)(Satellite *) = &getCurrentState;
