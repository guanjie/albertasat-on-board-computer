/*
 * Copyright (C) 2015 Collin Cupido, Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file Science.c
 * @author Collin Cupido
 * @author Brendan Bruner
 * @date 2014-12-28
 *
 * Implements methods which override methods inherited from State class.
 */
#include <stdio.h>

#include "Satellite.h"
#include "Science.h"
#include "EPS.h"

/*****************************************************************************/
/* Methods																	 */
/*****************************************************************************/
/**
 * @brief
 * 		Collects the WOD as required by Science mode.
 * @param obj
 * 		A pointer to the object invoking this method.
 * @param sat
 * 		A pointer to the Satellite object maintaining state information.
 * @remark
 * 		Thread safe.
 * 		Non reentrant.
 * @sa State::collectWOD
 * @public @memberof Science
 */
static void collectWOD(State *obj, Satellite *sat)
{
	int voltage, threshold;
	EPS *eps;

	eps = sat->getEPSDriver(sat);
	voltage = eps->getHouseKeeping(eps);
	threshold = ((PowerSafe *) obj)->getSwitchingVoltage((PowerSafe *) obj);

	printf("%i mV\n",voltage);

	if( voltage <= threshold )
	{
		sat->setCurrentState(sat, sat->getPowerSafeState(sat));
		printf("Battery Voltage Dropping: Going to PowerSafe Mode.\n");
	}
	else
	{
		printf("Battery Voltage Good: Science Mode Continues.\n");
	}
}

/**
 * @brief
 * 		Returns the voltage at which a transition to PowerSafe state must be
 * 		done.
 * @param obj
 * 		A pointer to the Science object invoking this method
 * @return
 * 		The voltage level when a transistion to PowerSafe state must be done
 * @public @memberof Science
 */
static int getSwitchingVoltage(Science *obj)
{
	return obj->data._switchingVoltage;
}

/**
 * @brief
 * 		Executes a hard telecommand as required by Science mode.
 * @param obj
 * 		A pointer to the object invoking this method.
 * @param sat
 * 		A pointer to the Satellite object maintaining state information.
 * @param sup
 * 		A pointer to the TelecommandSupervisor object which invoked this method.
 * @remark
 * 		Thread safe.
 * 		Non reentrant.
 * @sa State::executeHardTelecommand
 * @public @memberof Science
 */
static void executeHardTelecommand(State *obj, Command *command,  Satellite *sat, TelecommandSupervisor *sup)
{
	printf("Hard telecommand executed by Science mode. \n");
	command->execute(command, sat, sup);
}

/*****************************************************************************/
/* Constructor																 */
/*****************************************************************************/
Constructor(Science)
{
	Super(State);
	OverrideMethod(State, collectWOD);
	OverrideMethod(State, executeHardTelecommand);
	LinkMethod(getSwitchingVoltage);

	((State *) obj)->data._testID = SCIENCE_TEST_ID;
	obj->data._switchingVoltage = 16100;
}
