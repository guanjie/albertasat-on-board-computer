/*
 * Copyright (C) 2015 Collin Cupido
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file TeleCommandSupervisor.c
 * @author Collin Cupido
 * @date 2015-01-14
 *
 * Implements methods of TeleCommandSupervisor class.
 */

#include "TelecommandSupervisor.h"
#include "Command.h"
#include "MacroCommand.h"
#include "CommandCollectWOD.h"
#include "CommandSetStatePowerSafe.h"
#include "Satellite.h"
#include <stdio.h>
#include <string.h>




/*****************************************************************************/
/* Methods																	 */
/*****************************************************************************/
/**
 * @brief
 * 		Invokes the Satellite objects Satellite::executeSoftTelecommand() or
 * 		Satellite::executeHardTelecommand() methods if commands are
 * 		available. Runs with ms period set with this objects setPeriod method.
 * @remark
 * 		This is a protected method - it must not be called from the application.
 * @protected @memberof TelecommandSupervisor
 */
static void _supervisorTask(Supervisor *obj)
{
	TelecommandSupervisor *derived = (TelecommandSupervisor *) obj;

	struct telecommand_packet_t telepacket;
	Satellite *sat;
	COMM *comm;
	MacroCommand softCommand, hardCommand;
	Command command;

	sat = obj->getSatellite(obj);
	comm = sat->getCOMMDriver(sat);
	newMacroCommand(&softCommand);
	newMacroCommand(&hardCommand);

	for(;;)
	{
		telepacket = comm->getCommand(comm);

		/* TODO Implement a priority checker to decide
		 *  hard or soft commands from priority.
		 */
		if(strcmp(telepacket.telecommand, "none") != 0)
		{
			/* TODO Implement a database or lookup table to properly assign
			 * the necessary command - like CommandCollectWOD - to the
			 * newCommand function. Do this by making commands
			 * a member of the Command object?
			 */

			/* TODO The executeHardTelecommand is not compatible with the
			 * separate objects for each command. The following works,
			 * but gives compiler errors when &command is used in
			 * the constructor for the specific commands.
			 */
			if(strncmp(telepacket.telecommand, "collectWOD", 10) == 0)
			{
				newCommandCollectWOD((CommandCollectWOD *) &command);
				printf("Hard telecommand received! \n");
				sat->executeHardTelecommand(sat, &command, derived);
			}
			else if(strncmp(telepacket.telecommand, "setStatePowerSafe", 17) == 0)
			{
				newCommandSetStatePowerSafe((CommandSetStatePowerSafe *) &command);
				printf("Hard telecommand received! \n");
				sat->executeHardTelecommand(sat, &command, derived);
			}
			else{
				printf("Invalid Command received. Ignoring. \n");
			}
		}
		else
		{
			printf("No command yet. \n");
		}
		vSupervisorTaskDelay(obj->data._period);
	}
}

/*****************************************************************************/
/* Constructor																 */
/*****************************************************************************/
/**
 * @public @memberof TelecommandSupervisor
 */
Constructor(TelecommandSupervisor)
{
	Super(Supervisor);
	OverrideMethod(Supervisor, _supervisorTask);
}
