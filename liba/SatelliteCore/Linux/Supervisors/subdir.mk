################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Supervisors/Supervisor.c \
../Supervisors/TelecommandSupervisor.c \
../Supervisors/WODSupervisor.c 

OBJS += \
./Supervisors/Supervisor.o \
./Supervisors/TelecommandSupervisor.o \
./Supervisors/WODSupervisor.o 

C_DEPS += \
./Supervisors/Supervisor.d \
./Supervisors/TelecommandSupervisor.d \
./Supervisors/WODSupervisor.d 


# Each subdirectory must supply rules for building sources it contributes
Supervisors/%.o: ../Supervisors/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -D__REDLIB__ -I"/home/brendan/AlbertaSat/firmware/git/Simulation/liba/SatelliteCore/include" -I"/home/brendan/AlbertaSat/firmware/git/Simulation/liba/libDrivers/includes" -I"/home/brendan/AlbertaSat/firmware/git/Simulation/lib3rdParty/FreeRTOS/include" -I"/home/brendan/AlbertaSat/firmware/git/Simulation/lib3rdParty/FreeRTOS/portable/GCC/Posix" -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


