################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Drivers/COMM.c \
../Drivers/EPS.c 

OBJS += \
./Drivers/COMM.o \
./Drivers/EPS.o 

C_DEPS += \
./Drivers/COMM.d \
./Drivers/EPS.d 


# Each subdirectory must supply rules for building sources it contributes
Drivers/%.o: ../Drivers/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -D__REDLIB__ -I"/home/brendan/AlbertaSat/firmware/git/Simulation/prototypes/SatelliteReaperSimulation/SatelliteCore/include" -I"/home/brendan/AlbertaSat/firmware/git/Simulation/lib3rdParty/libFreeRTOSPosix/include" -I"/home/brendan/AlbertaSat/firmware/git/Simulation/lib3rdParty/libFreeRTOSPosix/FreeRTOS_Kernel/portable/GCC/Posix" -I"/home/brendan/AlbertaSat/firmware/git/Simulation/prototypes/SatelliteReaperSimulation/Application/AsyncIO" -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


