################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Commands/Command.c \
../Commands/CommandCollectWOD.c \
../Commands/CommandSetStatePowerSafe.c \
../Commands/MacroCommand.c 

OBJS += \
./Commands/Command.o \
./Commands/CommandCollectWOD.o \
./Commands/CommandSetStatePowerSafe.o \
./Commands/MacroCommand.o 

C_DEPS += \
./Commands/Command.d \
./Commands/CommandCollectWOD.d \
./Commands/CommandSetStatePowerSafe.d \
./Commands/MacroCommand.d 


# Each subdirectory must supply rules for building sources it contributes
Commands/%.o: ../Commands/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -D__REDLIB__ -I"/home/brendan/AlbertaSat/firmware/git/Simulation/liba/SatelliteCore/include" -I"/home/brendan/AlbertaSat/firmware/git/Simulation/liba/libDrivers/includes" -I"/home/brendan/AlbertaSat/firmware/git/Simulation/lib3rdParty/FreeRTOS/include" -I"/home/brendan/AlbertaSat/firmware/git/Simulation/lib3rdParty/FreeRTOS/portable/GCC/Posix" -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


