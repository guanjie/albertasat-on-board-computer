################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../CoreStateMachine/Detumble.c \
../CoreStateMachine/PowerOn.c \
../CoreStateMachine/PowerSafe.c \
../CoreStateMachine/Satellite.c \
../CoreStateMachine/Science.c \
../CoreStateMachine/State.c 

OBJS += \
./CoreStateMachine/Detumble.o \
./CoreStateMachine/PowerOn.o \
./CoreStateMachine/PowerSafe.o \
./CoreStateMachine/Satellite.o \
./CoreStateMachine/Science.o \
./CoreStateMachine/State.o 

C_DEPS += \
./CoreStateMachine/Detumble.d \
./CoreStateMachine/PowerOn.d \
./CoreStateMachine/PowerSafe.d \
./CoreStateMachine/Satellite.d \
./CoreStateMachine/Science.d \
./CoreStateMachine/State.d 


# Each subdirectory must supply rules for building sources it contributes
CoreStateMachine/%.o: ../CoreStateMachine/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -D__REDLIB__ -I"/home/brendan/AlbertaSat/firmware/git/Simulation/liba/SatelliteCore/include" -I"/home/brendan/AlbertaSat/firmware/git/Simulation/liba/libDrivers/includes" -I"/home/brendan/AlbertaSat/firmware/git/Simulation/lib3rdParty/FreeRTOS/include" -I"/home/brendan/AlbertaSat/firmware/git/Simulation/lib3rdParty/FreeRTOS/portable/GCC/Posix" -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


