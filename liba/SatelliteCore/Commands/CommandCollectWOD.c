/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file CommandCollectWOD.c
 * @author Brendan Bruner
 * @date 2015-01-10
 */

#include "CommandCollectWOD.h"
#include "Satellite.h"
#include "TelecommandSupervisor.h"

/*****************************************************************************/
/* Methods																	 */
/*****************************************************************************/
/**
 * @brief
 * 		Invokes the collection of wod.
 * @details
 * 		Will invoke the collection of wod from the Satellite object passed
 * 		in as a function argument. It will only invoke the collection once.
 * @attention
 * 		There is no guarantee that wod will be collected. That is 100% dependent
 * 		on the current state of the Satellite object passed in.
 * @param obj
 * 		A pointer to the Command object invoking this method.
 * @param sat
 * 		A pointer to the Satellite object for this command to act upon.
 * @param sup
 * 		Unused for this command. Pass as NULL.
 * @sa Command::execute()
 * @public @memberof CommandCollectWOD
 */
static void execute(Command *obj, Satellite *sat, TelecommandSupervisor *sup)
{
	sat->collectWOD(sat);
}

/*****************************************************************************/
/* Constructor																 */
/*****************************************************************************/
/**
 * @brief
 * 		Constructor for CommandCollectWOD
 * @public @memberof CommandCollectWOD
 */
Constructor(CommandCollectWOD)
{
	Super(Command);
	OverrideMethod(Command, execute);
}
