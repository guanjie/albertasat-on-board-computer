/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file Test_TelecommandSupervisor.h
 * @author Brendan Bruner
 * @date 2015-01-10
 */

#ifndef TEST_TELECOMMANDSUPERVISOR_H_
#define TEST_TELECOMMANDSUPERVISOR_H_

#include "unit.h"

TEST_SUITE(TelecommandSupervisor);

#endif /* TEST_TELECOMMANDSUPERVISOR_H_ */
