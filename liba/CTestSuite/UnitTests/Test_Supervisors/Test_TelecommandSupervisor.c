/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file Test_TelecommandSupervisor.c
 * @author Brendan Bruner
 * @date 2015-01-10
 */

#include "Test_TelecommandSupervisor.h"
#include "SupervisorMockUp.h"
#include "TelecommandSupervisor.h"
#include "StateMockUp.h"
#include "COMMSoftMockUp.h"
#include "COMMHardMockUp.h"

#define TELECOMMAND_SUPERVISOR_STACK 128

/*
 * COMMSoftMockUp generates arbitrary soft commands. This Comm mock up is set in the Satellite
 * object and the Satellite object is set in the TelecommandSupervisor. What should
 * happen is:
 * 		* The TelecommandSupervisor pends on the COMMMockUp for Commands.
 * 		* Upon receipt of a command it calls Satellite::executeSoftTelecommand().
 * 		* In the StateMockUp, the executeSoftTelecommand count is increased.
 * 		* We ASSERT that this count has increased to verify the TelecommandSupevisor
 * 		  is building and requesting the execution of commands.
 */
TEST(executes_soft_commands)
{
	COMMSoftMockUp comm;
	StateMockUp state;
	Satellite sat;
	EPS eps;
	TelecommandSupervisor sup;

	newCOMM((COMM *) &comm);
	newStateMockUp(&state);
	newSatellite(&sat);
	newEPS(&eps);
	newTelecommandSupervisor(&sup);

	sat.setCurrentState(&sat, (State *) &state);
	sat.setCOMMDriver(&sat, (COMM *) &comm);
	sat.setEPSDriver(&sat, &eps);
	((Supervisor *) &sup)->setPeriod((Supervisor *) &sup, (period_t) 25);
	((Supervisor *) &sup)->setSatellite((Supervisor *) &sup, &sat);

	ASSERT("execute_soft_commands 1", state.executeSoftTelecommandCount(&state) == 0);

	((Supervisor *) &sup)->initSupervisorTask((Supervisor *) &sup,
												"Telecommand supervisor test",
												TELECOMMAND_SUPERVISOR_STACK,
												(tskIDLE_PRIORITY + 1 ));
	vTaskDelay( 100 / portTICK_RATE_MS );

	ASSERT("execute_soft_commands 2", state.executeSoftTelecommandCount(&state) > 0);

	vTaskDelete(*((Supervisor *) &sup)->getTaskHandle((Supervisor *) &sup));

	return TEST_END;
}

/*
 * Same as executes_soft_commands test, except this tests that hard commands
 * are executed.
 */
TEST(executes_hard_commands)
{
	COMMHardMockUp comm;
	StateMockUp state;
	Satellite sat;
	EPS eps;
	TelecommandSupervisor sup;

	newCOMM((COMM *) &comm);
	newStateMockUp(&state);
	newSatellite(&sat);
	newEPS(&eps);
	newTelecommandSupervisor(&sup);

	sat.setCurrentState(&sat, (State *) &state);
	sat.setCOMMDriver(&sat, (COMM *) &comm);
	sat.setEPSDriver(&sat, &eps);
	((Supervisor *) &sup)->setPeriod((Supervisor *) &sup, (period_t) 25);
	((Supervisor *) &sup)->setSatellite((Supervisor *) &sup, &sat);

	ASSERT("execute_soft_commands 1", state.executeSoftTelecommandCount(&state) == 0);

	((Supervisor *) &sup)->initSupervisorTask((Supervisor *) &sup,
												"Telecommand supervisor test",
												TELECOMMAND_SUPERVISOR_STACK,
												(tskIDLE_PRIORITY + 1 ));
	vTaskDelay( 100 / portTICK_RATE_MS );

	ASSERT("execute_soft_commands 2", state.executeSoftTelecommandCount(&state) > 0);

	vTaskDelete(*((Supervisor *) &sup)->getTaskHandle((Supervisor *) &sup));

	return TEST_END;
}

TEST_SUITE(TelecommandSupervisor)
{
	ADD_TEST(executes_soft_commands);
	ADD_TEST(executes_hard_commands);
	return TEST_END;
}
