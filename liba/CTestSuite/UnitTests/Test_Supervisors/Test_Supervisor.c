/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file Test_Supervisor.c
 * @author Brendan Bruner
 * @date 2015-01-07
 */

#include "Test_Supervisor.h"
#include "SupervisorMockUp.h"

TEST(set_satellite)
{
	Supervisor sup;
	Satellite *testSat;

	newSupervisor(&sup);
	testSat = (Satellite *) 0x892;
	sup.setSatellite(&sup, testSat);

	ASSERT("set_satellite", sup.getSatellite(&sup) == testSat);

	return TEST_END;
}

TEST(set_period)
{
	Supervisor sup;
	period_t period;

	newSupervisor(&sup);
	period = (period_t) 278;
	sup.setPeriod(&sup, period);

	ASSERT("set_period", sup.data._period == period);

	return TEST_END;
}

TEST(correct_task_handle)
{
	SupervisorMockUp supM;

	newSupervisorMockUp(&supM);

	((Supervisor *) &supM)->initSupervisorTask((Supervisor *) &supM,
												"Task handle test",
												SUPERVISOR_MOCK_UP_STACK,
												SUPERVISOR_MOCK_UP_PRIO);
	ASSERT("correct_task_handle",
			((Supervisor *) &supM)->getTaskHandle((Supervisor *) &supM) == &((Supervisor *) &supM)->data._taskHandle);

	supM.kill(&supM);

	return TEST_END;
}

TEST(stop_and_start_task)
{
	SupervisorMockUp supM;

	newSupervisorMockUp(&supM);

	ASSERT("stop_and_start_task 1", supM.data.counter == 0);

	((Supervisor *) &supM)->initSupervisorTask((Supervisor *) &supM,
												"Start and stop test",
												SUPERVISOR_MOCK_UP_STACK,
												SUPERVISOR_MOCK_UP_PRIO);

	/* Assert the task started and is running */
	vTaskDelay(50 / portTICK_RATE_MS);
	ASSERT("stop_and_start_task 2", supM.data.counter > 0);

	/* Assert the task was stopped */
	((Supervisor *) &supM)->stop((Supervisor *) &supM);
	supM.data.counter = 0;
	vTaskDelay(50 / portTICK_RATE_MS);
	ASSERT("stop_and_start_task 3", supM.data.counter == 0);

	((Supervisor *) &supM)->start((Supervisor *) &supM);
	vTaskDelay(50 / portTICK_RATE_MS);
	ASSERT("stop_and_start_task 4", supM.data.counter > 0);

	supM.kill(&supM);

	return TEST_END;
}


TEST_SUITE(Supervisor)
{
	ADD_TEST(set_satellite);
	ADD_TEST(set_period);
	ADD_TEST(correct_task_handle);
	ADD_TEST(stop_and_start_task);
	return TEST_END;
}
